﻿using Project_Structure.DAL.Interfaces;
using Project_Structure.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text.Json;

namespace Project_Structure.DAL.Repositories
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : Entity
    {
        private readonly List<TEntity> _entities = new List<TEntity>();
        public Repository() { }
        public Repository(string json)
        {
            _entities = JsonSerializer.Deserialize<List<TEntity>>(json, new JsonSerializerOptions
            {
                PropertyNameCaseInsensitive = true
            });
        }

        public IEnumerable<TEntity> Get(Expression<Func<TEntity, bool>> filter = null)
        {
            List<TEntity> entities = new List<TEntity>();
            if (filter != null)
            {
                entities.AddRange(_entities.AsQueryable().Where(filter).Select(i => (TEntity)i.Copy()));
            }
            else
            {
                entities.AddRange(_entities.Select(i => (TEntity)i.Copy()));
            }
            return entities;
        }
        public void Create(TEntity entity)
        {
            _entities.Add(entity);
        }
        public void Update(TEntity entity)
        {
            var update = _entities.FirstOrDefault(e => e.Id == entity.Id);
            if (update != null) 
            { 
                Delete(update);
                Create(entity);
            }
        }
        public void Delete(int id)
        {
            var delete = _entities.FirstOrDefault(e => e.Id == id);
            if (delete != null)
            {
                Delete(delete);
            }
        }
        public void Delete(TEntity entity)
        {
            _entities.Remove(entity);
        }
    }
}

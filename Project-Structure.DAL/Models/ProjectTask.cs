﻿using System;

namespace Project_Structure.DAL.Models
{
    public class ProjectTask : Entity
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime FinishedAt { get; set; }
        public TaskState State { get; set; }
        public int ProjectId { get; set; }
        public int PerformerId { get; set; }
        public User Performer { get; set; }
        public ProjectTask() : base() { }
        public ProjectTask(int id, string name, string description, DateTime createdAt, DateTime finishedAt, TaskState state, int projectId, int performerId) : base(id)
        {
            Name = name;
            Description = description;
            CreatedAt = createdAt;
            FinishedAt = finishedAt;
            State = state;
            ProjectId = projectId;
            PerformerId = performerId;
        }
        public override Entity Copy()
        {
            return new ProjectTask(this.Id, this.Name, this.Description, this.CreatedAt, this.FinishedAt, this.State, this.ProjectId, this.PerformerId);
        }
    }
}

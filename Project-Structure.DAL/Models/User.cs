﻿using System;

namespace Project_Structure.DAL.Models
{
    public class User : Entity
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public DateTime Birthday { get; set; }
        public DateTime RegisteredAt { get; set; }
        public int? TeamId { get; set; }
        public User(int id, string firstName, string lastName, string email, DateTime birthday, DateTime registeredAt, int? teamId) : base(id)
        {
            FirstName = firstName;
            LastName = lastName;
            Email = email;
            Birthday = birthday;
            RegisteredAt = registeredAt;
            TeamId = teamId;
        }
        public User() : base() { }
        public override Entity Copy()
        {
            return new User(this.Id, this.FirstName, this.LastName, this.Email, this.Birthday, this.RegisteredAt, this.TeamId);
        }
    }
}

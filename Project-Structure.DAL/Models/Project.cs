﻿using System;
using System.Collections.Generic;

namespace Project_Structure.DAL.Models
{
    public class Project : Entity
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime Deadline { get; set; }
        public int AuthorId { get; set; }
        public int TeamId { get; set; }
        public IEnumerable<ProjectTask> Tasks { get; set; }
        public User Author { get; set; }
        public Team Team { get; set; }
        public Project() : base() { }
        public Project(int id, string name, string description, DateTime createdAt, DateTime deadline, int authorId, int teamId) : base(id)
        {
            Name = name;
            Description = description;
            CreatedAt = createdAt;
            Deadline = deadline;
            AuthorId = authorId;
            TeamId = teamId;
        }
        public override Entity Copy()
        {
            return new Project(this.Id, this.Name, this.Description, this.CreatedAt, this.Deadline, this.AuthorId, this.TeamId);
        }
    }
}

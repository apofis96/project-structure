﻿namespace Project_Structure.DAL.Models
{
    public abstract class Entity
    {
        public int Id { get; set; }
        public Entity (int id)
        {
            Id = id;
        }
        public Entity()
        { }
        public abstract Entity Copy();
    }
}

﻿using System;

namespace Project_Structure.DAL.Models
{
    public class Team : Entity
    {
        public string Name { get; set; }
        public DateTime CreatedAt { get; set; }
        public Team() : base() { }
        public Team(int id, string name, DateTime createdAt) : base(id) 
        {
            Name = name;
            CreatedAt = createdAt;
        }
        public override Entity Copy()
        {
            return new Team(this.Id, this.Name, this.CreatedAt);
        }
    }
}

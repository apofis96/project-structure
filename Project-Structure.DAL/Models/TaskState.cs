﻿namespace Project_Structure.DAL.Models
{
    public enum TaskState
    {
        Created,
        Started,
        Finished,
        Canceled
    }
}

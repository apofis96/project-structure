﻿using Project_Structure.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Project_Structure.DAL.Interfaces
{
    public interface IRepository<TEntity> where TEntity : Entity
    {
        IEnumerable<TEntity> Get(Expression<Func<TEntity, bool>> filter = null);
        void Create(TEntity entity);
        void Update(TEntity entity);
        void Delete(int id);
        void Delete(TEntity entity);
        static Expression<Func<TEntity, bool>> FilterById(int id)
        {
            return x => x.Id == id;
        }
    }
}

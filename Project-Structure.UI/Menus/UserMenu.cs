﻿using Project_Structure.BLL.DTO.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace Project_Structure.UI.Menus
{
    public class UserMenu
    {
        public static async Task menu(HttpClient client, JsonSerializerOptions options)
        {
            string warning = "Sequence output is limited to three objects!";
            string pause = "Press any key to continue";
            string subMenuItems = @"1 : ReadAll
2 : Read(id)
3 : Create
4 : Update
5 : Delete
0 : Exit.";
            string subpath = "Users";
            Console.WriteLine(subMenuItems);
            bool loop = true;
            while (loop)
            {
                Console.Write("User menu input: ");
                char menu = Console.ReadKey().KeyChar;
                Console.WriteLine("\n================================");
                switch (menu)
                {
                    case '1':
                        Console.WriteLine(warning);
                        Console.WriteLine(pause);
                        Console.ReadKey();
                        Console.WriteLine(JsonSerializer.Serialize(JsonSerializer.Deserialize<IEnumerable<UserDTO>>(await client.GetStringAsync(subpath), options).Take(3), options));
                        break;
                    case '2':
                        Console.WriteLine(JsonSerializer.Serialize(JsonSerializer.Deserialize<UserDTO>(await client.GetStringAsync(subpath + "/" + MenuInputs.IdInput()), options), options));
                        break;
                    case '3':
                        var newUser = new UserCreateDTO();
                        Console.Write("FirstName: ");
                        newUser.FirstName = Console.ReadLine();
                        Console.Write("LastName: ");
                        newUser.LastName = Console.ReadLine();
                        Console.Write("Email: ");
                        newUser.Email = Console.ReadLine();
                        Console.Write("Birthday: ");
                        newUser.Birthday = MenuInputs.DateTimeInput();
                        Console.Write("Press \"0\" for TeamId input: ");
                        if (Console.ReadLine() == "0")
                        {
                            Console.Write("Team ");
                            newUser.TeamId = MenuInputs.IdInput();
                        }
                        HttpResponseMessage response = await client.PostAsync(subpath, new StringContent(JsonSerializer.Serialize(newUser), Encoding.UTF8, "application/json"));
                        if (response.StatusCode != HttpStatusCode.Created)
                        {
                            Console.WriteLine(response.StatusCode);
                            return;
                        }
                        Console.WriteLine(JsonSerializer.Serialize(JsonSerializer.Deserialize<UserDTO>(await response.Content.ReadAsStringAsync(), options), options));
                        Console.WriteLine("Done");
                        break;
                    case '4':
                        var updateUser = new UserUpdateDTO();
                        updateUser.Id = MenuInputs.IdInput();
                        Console.Write("FirstName: ");
                        updateUser.FirstName = Console.ReadLine();
                        Console.Write("LastName: ");
                        updateUser.LastName = Console.ReadLine();
                        Console.Write("Email: ");
                        updateUser.Email = Console.ReadLine();
                        Console.Write("Birthday: ");
                        updateUser.Birthday = MenuInputs.DateTimeInput();
                        Console.Write("Press \"0\" for TeamId input: ");
                        if (Console.ReadLine() == "0")
                        {
                            Console.Write("TeamId ");
                            updateUser.TeamId = MenuInputs.IdInput();
                        }
                        response = await client.PutAsync(subpath, new StringContent(JsonSerializer.Serialize(updateUser), Encoding.UTF8, "application/json"));
                        if (response.StatusCode != HttpStatusCode.Created)
                        {
                            Console.WriteLine(response.StatusCode);
                            return;
                        }
                        Console.WriteLine(JsonSerializer.Serialize(JsonSerializer.Deserialize<UserDTO>(await response.Content.ReadAsStringAsync(), options), options));
                        Console.WriteLine("Done");
                        break;
                    case '5':
                        response = await client.DeleteAsync(subpath + "/" + MenuInputs.IdInput());
                        if (response.StatusCode != HttpStatusCode.NoContent)
                        {
                            Console.WriteLine(response.StatusCode);
                            return;
                        }
                        Console.WriteLine("Done");
                        break;
                    case '0':
                        loop = false;
                        break;
                    default:
                        Console.WriteLine(subMenuItems);
                        break;
                }
            }
        }
    }
}

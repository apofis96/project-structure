﻿using System;

namespace Project_Structure.UI.Menus
{
    public class MenuInputs
    {
        public static int IdInput()
        {
            int input;
            do
            {
                Console.Write("Input ID: ");
                input = int.TryParse(Console.ReadLine(), out input) ? input : -1;
                if (input < 0)
                {
                    Console.WriteLine("Wrong input");
                }
                else
                    return input;
            }
            while (true);
        }
        public static DateTime DateTimeInput()
        {
            Console.WriteLine("Enter date in format {0}", DateTime.Now);
            DateTime result;
            while (!DateTime.TryParse(Console.ReadLine(), out result))
            {
                Console.WriteLine("Wrong input");
            }
            return result;
        }
    }
}

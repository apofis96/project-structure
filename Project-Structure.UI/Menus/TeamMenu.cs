﻿using Project_Structure.BLL.DTO.Team;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace Project_Structure.UI.Menus
{
    public class TeamMenu
    {
        public static async Task menu(HttpClient client, JsonSerializerOptions options)
        {
            string warning = "Sequence output is limited to three objects!";
            string pause = "Press any key to continue";
            string subMenuItems = @"1 : ReadAll
2 : Read(id)
3 : Create
4 : Update
5 : Delete
0 : Exit.";
            string subpath = "Teams";
            Console.WriteLine(subMenuItems);
            bool loop = true;
            while (loop)
            {
                Console.Write("Team menu input: ");
                char menu = Console.ReadKey().KeyChar;
                Console.WriteLine("\n================================");
                switch (menu)
                {
                    case '1':
                        Console.WriteLine(warning);
                        Console.WriteLine(pause);
                        Console.ReadKey();
                        Console.WriteLine(JsonSerializer.Serialize(JsonSerializer.Deserialize<IEnumerable<TeamDTO>>(await client.GetStringAsync(subpath), options).Take(3), options));
                        break;
                    case '2':
                        Console.WriteLine(JsonSerializer.Serialize(JsonSerializer.Deserialize<TeamDTO>(await client.GetStringAsync(subpath + "/" + MenuInputs.IdInput()), options), options));
                        break;
                    case '3':
                        var newTeam = new TeamCreateDTO();
                        Console.Write("Name: ");
                        newTeam.Name = Console.ReadLine();
                        HttpResponseMessage response = await client.PostAsync(subpath, new StringContent(JsonSerializer.Serialize(newTeam), Encoding.UTF8, "application/json"));
                        if (response.StatusCode != HttpStatusCode.Created)
                        {
                            Console.WriteLine(response.StatusCode);
                            return;
                        }
                        Console.WriteLine();
                        Console.WriteLine(JsonSerializer.Serialize(JsonSerializer.Deserialize<TeamDTO>(await response.Content.ReadAsStringAsync(), options), options));
                        Console.WriteLine("Done");
                        break;
                    case '4':
                        var updateTeam = new TeamUpdateDTO();
                        updateTeam.Id = MenuInputs.IdInput();
                        Console.Write("Name: ");
                        updateTeam.Name = Console.ReadLine();
                        response = await client.PutAsync(subpath, new StringContent(JsonSerializer.Serialize(updateTeam), Encoding.UTF8, "application/json"));
                        if (response.StatusCode != HttpStatusCode.Created)
                        {
                            Console.WriteLine(response.StatusCode);
                            return;
                        }
                        Console.WriteLine(JsonSerializer.Serialize(JsonSerializer.Deserialize<TeamDTO>(await response.Content.ReadAsStringAsync(), options), options));
                        Console.WriteLine("Done");
                        break;
                    case '5':
                        response = await client.DeleteAsync(subpath + "/" + MenuInputs.IdInput());
                        if (response.StatusCode != HttpStatusCode.NoContent)
                        {
                            Console.WriteLine(response.StatusCode);
                            return;
                        }
                        Console.WriteLine("Done");
                        break;
                    case '0':
                        loop = false;
                        break;
                    default:
                        Console.WriteLine(subMenuItems);
                        break;
                }
            }
        }
    }
}

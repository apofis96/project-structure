﻿using AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using Project_Structure.BLL.Interfaces;
using Project_Structure.BLL.MappingProfiles;
using Project_Structure.BLL.Services;
using Project_Structure.DAL.Interfaces;
using Project_Structure.DAL.Models;
using Project_Structure.DAL.Repositories;
using System.IO;
using System.Reflection;

namespace Project_Structure.WebAPI.Extensions
{
    public static class ServiceExtensions
    {
        public static void RegisterCustomServices(this IServiceCollection services)
        {
            services.AddSingleton<IRepository<User>>(sp => new Repository<User>(
                File.ReadAllText("PreloadedData\\users.json")
                ));
            services.AddSingleton<IRepository<Team>>(sp => new Repository<Team>(
                File.ReadAllText("PreloadedData\\teams.json")
                ));
            services.AddSingleton<IRepository<ProjectTask>>(sp => new Repository<ProjectTask>(
                File.ReadAllText("PreloadedData\\tasks.json")
                ));
            services.AddSingleton<IRepository<Project>>(sp => new Repository<Project>(
                File.ReadAllText("PreloadedData\\projects.json")
                ));
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<ITeamService, TeamService>();
            services.AddScoped<IProjectTaskService, ProjectTaskService>();
            services.AddScoped<IProjectService, ProjectService>();
        }
        public static void RegisterAutoMapper(this IServiceCollection services)
        {
            services.AddAutoMapper(cfg =>
            {
                cfg.AddProfile<UserProfile>();
                cfg.AddProfile<TeamProfile>();
                cfg.AddProfile<ProjectTaskProfile>();
                cfg.AddProfile<ProjectProfile>();
            },
            Assembly.GetExecutingAssembly());
        }
    }
}

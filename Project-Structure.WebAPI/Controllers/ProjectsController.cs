﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Project_Structure.BLL.DTO.Project;
using Project_Structure.BLL.DTO.QueryResults;
using Project_Structure.BLL.Interfaces;

namespace Project_Structure.WebAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ProjectsController : ControllerBase
    {
        private readonly IProjectService _projectService;
        public ProjectsController(IProjectService projectService)
        {
            _projectService = projectService;
        }
        [HttpGet]
        public IEnumerable<ProjectDTO> Get()
        {
            return _projectService.GetAll();
        }
        [HttpGet("{id}", Name = "ProjectGet")]
        public ActionResult<ProjectDTO> Get(int id)
        {
            try
            {
                return Ok(_projectService.Get(id));
            }
            catch (InvalidOperationException)
            {
                return NotFound();
            }
            catch (ArgumentException)
            {
                return BadRequest();
            }
        }
        [HttpPost]
        public ActionResult<ProjectDTO> Post([FromBody] ProjectCreateDTO project)
        {
            try
            {
                var newProject = _projectService.Post(project);
                return CreatedAtRoute("ProjectGet", new { newProject.Id }, newProject);
            }
            catch (InvalidOperationException)
            {
                return NotFound();
            }
            catch (ArgumentException)
            {
                return BadRequest();
            }
        }
        [HttpPut]
        public ActionResult<ProjectDTO> Put([FromBody] ProjectUpdateDTO project)
        {
            try
            {
                var updateProject = _projectService.Update(project);
                return CreatedAtRoute("ProjectGet", new { updateProject.Id }, updateProject);
            }
            catch (InvalidOperationException)
            {
                return NotFound();
            }
            catch (ArgumentException)
            {
                return BadRequest();
            }
        }
        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            try
            {
                _projectService.Delete(id);
                return NoContent();
            }
            catch (InvalidOperationException)
            {
                return NotFound();
            }
            catch (ArgumentException)
            {
                return BadRequest();
            }
        }
        [HttpGet("TasksCountByUser/{id}")]
        public ActionResult<IEnumerable<TaskCountDTO>> GetTasksCountByUser(int id)
        {
            try
            {
                return Ok(_projectService.GetTasksCountByUser(id));
            }
            catch (InvalidOperationException)
            {
                return NotFound();
            }
            catch (ArgumentException)
            {
                return BadRequest();
            }
        }
        [HttpGet("UserStatistics/{id}")]
        public ActionResult<UserStatisticDTO> GetUserStatistics(int id)
        {
            try
            {
                return Ok(_projectService.GetUserStatistics(id));
            }
            catch (InvalidOperationException)
            {
                return NotFound();
            }
            catch (ArgumentException)
            {
                return BadRequest();
            }
        }
        [HttpGet("ProjectStatistics")]
        public IEnumerable<ProjectStatisticDTO> GetProjectStatistics()
        {
            return _projectService.GetProjectStatistics();
        }

    }
}

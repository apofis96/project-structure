﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Project_Structure.BLL.DTO.ProjectTask;
using Project_Structure.BLL.DTO.QueryResults;
using Project_Structure.BLL.Interfaces;

namespace Project_Structure.WebAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class TasksController : ControllerBase
    {
        private readonly IProjectTaskService _taskService;
        public TasksController(IProjectTaskService taskService)
        {
            _taskService = taskService;
        }
        [HttpGet]
        public IEnumerable<ProjectTaskDTO> Get()
        {
            return _taskService.GetAll();
        }
        [HttpGet("{id}", Name = "TaskGet")]
        public ActionResult<ProjectTaskDTO> Get(int id)
        {
            try
            {
                return Ok(_taskService.Get(id));
            }
            catch (InvalidOperationException)
            {
                return NotFound();
            }
            catch (ArgumentException)
            {
                return BadRequest();
            }
        }
        [HttpPost]
        public ActionResult<ProjectTaskDTO> Post([FromBody] ProjectTaskCreateDTO task)
        {
            try
            {
                var newTask = _taskService.Post(task);
                return CreatedAtRoute("TaskGet", new { newTask.Id }, newTask);
            }
            catch (InvalidOperationException)
            {
                return NotFound();
            }
            catch (ArgumentException)
            {
                return BadRequest();
            }
        }
        [HttpPut]
        public ActionResult<ProjectTaskDTO> Put([FromBody] ProjectTaskUpdateDTO task)
        {
            try
            {
                var updateTask = _taskService.Update(task);
                return CreatedAtRoute("TaskGet", new { updateTask.Id }, updateTask);
            }
            catch (InvalidOperationException)
            {
                return NotFound();
            }
            catch (ArgumentException)
            {
                return BadRequest();
            }
        }
        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            try
            {
                _taskService.Delete(id);
                return NoContent();
            }
            catch (InvalidOperationException)
            {
                return NotFound();
            }
            catch (ArgumentException)
            {
                return BadRequest();
            }
        }
        [HttpGet("TasksByUser/{id}")]
        public ActionResult<IEnumerable<ProjectTaskDTO>> GetTasksCountByUser(int id)
        {
            try
            {
                return Ok(_taskService.GetTasksByUser(id));
            }
            catch (InvalidOperationException)
            {
                return NotFound();
            }
            catch (ArgumentException)
            {
                return BadRequest();
            }
        }
        [HttpGet("FinishedTasks/{id}")]
        public ActionResult<IEnumerable<FinishedTaskDTO>> GetFinishedTasks(int id)
        {
            try
            {
                return Ok(_taskService.GetFinishedTasks(id));
            }
            catch (InvalidOperationException)
            {
                return NotFound();
            }
            catch (ArgumentException)
            {
                return BadRequest();
            }
        }
    }
}

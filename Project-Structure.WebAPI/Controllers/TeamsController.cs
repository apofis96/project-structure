﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Project_Structure.BLL.DTO.QueryResults;
using Project_Structure.BLL.DTO.Team;
using Project_Structure.BLL.Interfaces;

namespace Project_Structure.WebAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class TeamsController : ControllerBase
    {
        private readonly ITeamService _teamService;
        public TeamsController(ITeamService teamService)
        {
            _teamService = teamService;
        }
        [HttpGet]
        public IEnumerable<TeamDTO> Get()
        {
            return _teamService.GetAll();
        }
        [HttpGet("{id}", Name = "TeamGet")]
        public ActionResult<TeamDTO> Get(int id)
        {
            try
            {
                return Ok(_teamService.Get(id));
            }
            catch (InvalidOperationException)
            {
                return NotFound();
            }
            catch (ArgumentException)
            {
                return BadRequest();
            }
        }
        [HttpPost]
        public ActionResult<TeamDTO> Post([FromBody] TeamCreateDTO team)
        {
            try
            {
                var newTeam = _teamService.Post(team);
                return CreatedAtRoute("TeamGet", new { newTeam.Id }, newTeam);
            }
            catch (InvalidOperationException)
            {
                return NotFound();
            }
            catch (ArgumentException)
            {
                return BadRequest();
            }
        }
        [HttpPut]
        public ActionResult<TeamDTO> Put([FromBody] TeamUpdateDTO team)
        {
            try
            {
                var updateTeam = _teamService.Update(team);
                return CreatedAtRoute("TeamGet", new { updateTeam.Id }, updateTeam);
            }
            catch (InvalidOperationException)
            {
                return NotFound();
            }
            catch (ArgumentException)
            {
                return BadRequest();
            }
        }
        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            try
            {
            _teamService.Delete(id);
                return NoContent();
            }
            catch (InvalidOperationException)
            {
                return NotFound();
            }
            catch (ArgumentException)
            {
                return BadRequest();
            }
        }
        [HttpGet("TeamMembers")]
        public ActionResult<IEnumerable<TeamMembersDTO>> GetTeamMembers()
        {
            return Ok(_teamService.GetTeamMembers());
        }
    }
}

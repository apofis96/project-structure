﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Project_Structure.BLL.DTO.QueryResults;
using Project_Structure.BLL.DTO.User;
using Project_Structure.BLL.Interfaces;

namespace Project_Structure.WebAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UsersController : ControllerBase
    {
        private readonly IUserService _userService;
        public UsersController(IUserService userService)
        {
            _userService = userService;
        }
        [HttpGet]
        public IEnumerable<UserDTO> Get()
        {
            return _userService.GetAll();
        }
        [HttpGet("{id}", Name = "UserGet")]
        public ActionResult<UserDTO> Get(int id)
        {
            try
            {
                return Ok(_userService.Get(id));
            }
            catch (InvalidOperationException)
            {
                return NotFound();
            }
            catch (ArgumentException)
            {
                return BadRequest();
            }
        }
        [HttpPost]
        public ActionResult<UserDTO> Post([FromBody] UserCreateDTO user)
        {
            try
            {
                var newUser = _userService.Post(user);
                return CreatedAtRoute("UserGet", new { newUser.Id }, newUser);
            }
            catch (InvalidOperationException)
            {
                return NotFound();
            }
            catch (ArgumentException)
            {
                return BadRequest();
            }
        }
        [HttpPut]
        public ActionResult<UserDTO> Put([FromBody] UserUpdateDTO user)
        {
            try
            {
                var updateUser = _userService.Update(user);
                return CreatedAtRoute("UserGet", new { updateUser.Id }, updateUser);
            }
            catch (InvalidOperationException)
            {
                return NotFound();
            }
            catch (ArgumentException)
            {
                return BadRequest();
            }
        }
        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            try
            {
                _userService.Delete(id);
                return NoContent();
            }
            catch (InvalidOperationException)
            {
                return NotFound();
            }
            catch (ArgumentException)
            {
                return BadRequest();
            }
        }
        [HttpGet("UserTasks")]
        public ActionResult<IEnumerable<UserTasksDTO>> GetUserTasks()
        {
            return Ok(_userService.GetUserTasks());
        }
    }
}

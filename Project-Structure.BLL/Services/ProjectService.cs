﻿using AutoMapper;
using Project_Structure.BLL.DTO.Project;
using Project_Structure.BLL.DTO.ProjectTask;
using Project_Structure.BLL.DTO.QueryResults;
using Project_Structure.BLL.Interfaces;
using Project_Structure.DAL.Interfaces;
using Project_Structure.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Project_Structure.BLL.Services
{
    public class ProjectService : IProjectService
    {
        private readonly IRepository<Project> _projectRepository;
        private readonly IRepository<ProjectTask> _projectTaskRepository;
        private readonly IRepository<User> _userRepository;
        private readonly IRepository<Team> _teamRepository;
        private readonly IMapper _mapper;
        public ProjectService(IRepository<Project> projectRepository, IRepository<ProjectTask> projectTaskRepository, IRepository<User> userRepository, IRepository<Team> teamRepository, IMapper mapper)
        {
            _projectRepository = projectRepository;
            _projectTaskRepository = projectTaskRepository;
            _userRepository = userRepository;
            _teamRepository = teamRepository;
            _mapper = mapper;
        }
        public IEnumerable<ProjectDTO> GetAll()
        {
            var projects = _projectRepository.Get().GroupJoin(_projectTaskRepository.Get().GroupJoin(_userRepository.Get(),
                t => t.PerformerId, u => u.Id, (t, u) =>
                {
                    t.Performer = u.FirstOrDefault();
                    return t;
                }),
                p => p.Id, ts => ts.ProjectId, (p, ts) =>
                {
                    p.Tasks = ts.ToList(); ;
                    return p;
                }).GroupJoin(_userRepository.Get(),
                p => p.AuthorId, u => u.Id, (p, u) =>
                {
                    p.Author = u.FirstOrDefault();
                    return p;
                }).GroupJoin(_teamRepository.Get(),
                p => p.TeamId, t => t.Id, (p, t) =>
                {
                    p.Team = t.FirstOrDefault();
                    return p;
                });
            return _mapper.Map<IEnumerable<ProjectDTO>>(projects);
        }
        public ProjectDTO Get(int id)
        {
            var project = _projectRepository.Get(IRepository<Project>.FilterById(id)).FirstOrDefault();
            if (project == null)
                throw new InvalidOperationException("Project not found");
            project.Author = _userRepository.Get(IRepository<User>.FilterById(project.AuthorId)).FirstOrDefault();
            project.Tasks = _projectTaskRepository.Get().Where(t => t.ProjectId == project.Id).GroupJoin(_userRepository.Get(),
            t => t.PerformerId, u => u.Id, (t, u) =>
            {
                t.Performer = u.FirstOrDefault();
                return t;
            });
            project.Team = _teamRepository.Get(IRepository<Team>.FilterById(project.TeamId)).FirstOrDefault();
            return _mapper.Map<ProjectDTO>(project);
        }
        public ProjectDTO Post(ProjectCreateDTO project)
        {
            if (!UserIsExist(project.AuthorId))
                throw new ArgumentException("User not found");
            if (!TeamIsExist(project.TeamId))
                throw new ArgumentException("Team not found");
            int newId = _projectRepository.Get().OrderByDescending(u => u.Id).Select(u => u.Id).FirstOrDefault() + 1;
            var newProject = new Project(newId, project.Name, project.Description, DateTime.Now, project.Deadline, project.AuthorId, project.TeamId);
            _projectRepository.Create(newProject);
            return _mapper.Map<ProjectDTO>(newProject);
        }
        public ProjectDTO Update(ProjectUpdateDTO updateProject)
        {
            if (!UserIsExist(updateProject.AuthorId))
                throw new ArgumentException("User not found");
            if (!TeamIsExist(updateProject.TeamId))
                throw new ArgumentException("Team not found");
            var project = _projectRepository.Get(IRepository<Project>.FilterById(updateProject.Id)).FirstOrDefault();
            if(project != null)
            {
                project.Name = updateProject.Name;
                project.Description = updateProject.Description;
                project.Deadline = updateProject.Deadline;
                project.AuthorId = updateProject.AuthorId;
                project.TeamId = updateProject.TeamId;
                _projectRepository.Update(project);
            }
            return _mapper.Map<ProjectDTO>(Get(updateProject.Id));
        }
        public void Delete(int id)
        {
            if (Get(id) == null)
                throw new InvalidOperationException("Project not found");
            _projectRepository.Delete(id);
        }
        public IEnumerable<TaskCountDTO> GetTasksCountByUser(int userId)
        {
            if (!UserIsExist(userId))
                throw new ArgumentException("User not found");
            return _mapper.Map<IEnumerable<TaskCountDTO>>(GetAll().Where(p => p.Author?.Id == userId).ToDictionary(k => k, k => k.Tasks.Where(t => t.Performer?.Id == userId).Count())); ;
        }
        public UserStatisticDTO GetUserStatistics(int userId)
        {
            if (!UserIsExist(userId))
                throw new ArgumentException("User not found");
            var statistic = GetAll().Where(p =>p.Author?.Id == userId).OrderByDescending(p => p.CreatedAt).GroupJoin(_projectTaskRepository.Get().OrderByDescending(t => t.FinishedAt - t.CreatedAt),
                p => p.Author.Id, t => t.PerformerId, (p, t) => new UserStatisticDTO
                {
                    User = p.Author,
                    LastProject = p,
                    LastProjectTasksCount = p.Tasks.Count(),
                    NotFinishedTasksCount = t.Where(t => t.State != TaskState.Finished).Count(),
                    LongestTask = _mapper.Map<ProjectTaskDTO>(t.FirstOrDefault())
                }).FirstOrDefault();
            statistic.LongestTask.Performer = statistic.User;
            return statistic;
        }
        public IEnumerable<ProjectStatisticDTO> GetProjectStatistics()
        {
            return GetAll().Select(p => new ProjectStatisticDTO
            {
                Project = p,
                LongestDescriptionTask = p.Tasks.OrderByDescending(t => t.Description.Length).FirstOrDefault(),
                ShortestNameTask = p.Tasks.OrderBy(t => t.Name.Length).FirstOrDefault(),
                MembersTeamCount = _userRepository.Get().Where(u => u.TeamId == p.Team?.Id && (p.Description.Length > 20 || p.Tasks.Count() < 3)).Count()
            });
        }
        private bool UserIsExist(int id)
        {
            if (_userRepository.Get(IRepository<User>.FilterById(id)).FirstOrDefault() == null)
                return false;
            return true;
        }
        private bool TeamIsExist(int id)
        {
            if (_teamRepository.Get(IRepository<Team>.FilterById(id)).FirstOrDefault() == null)
                return false;
            return true;
        }
    }
}

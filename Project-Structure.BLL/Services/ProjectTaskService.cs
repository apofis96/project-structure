﻿using AutoMapper;
using Project_Structure.BLL.DTO.ProjectTask;
using Project_Structure.BLL.DTO.QueryResults;
using Project_Structure.BLL.Interfaces;
using Project_Structure.DAL.Interfaces;
using Project_Structure.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Project_Structure.BLL.Services
{
    public class ProjectTaskService : IProjectTaskService
    {
        private readonly IRepository<ProjectTask> _projectTaskRepository;
        private readonly IRepository<User> _userRepository;
        private readonly IRepository<Project> _projectRepository;
        private readonly IMapper _mapper;
        public ProjectTaskService(IRepository<ProjectTask> projectTaskRepository, IRepository<User> userRepository, IRepository<Project> projectRepository, IMapper mapper)
        {
            _projectTaskRepository = projectTaskRepository;
            _userRepository = userRepository;
            _projectRepository = projectRepository;
            _mapper = mapper;
        }
        public IEnumerable<ProjectTaskDTO> GetAll()
        {
            var tasks = _projectTaskRepository.Get();
            tasks = tasks.GroupJoin(_userRepository.Get(),
                t => t.PerformerId, u => u.Id, (t, u) =>
                 {
                     t.Performer = u.FirstOrDefault();
                     return t;
                 });
            return _mapper.Map<IEnumerable<ProjectTaskDTO>>(tasks);
        }
        public ProjectTaskDTO Get(int id)
        {
            var task = _projectTaskRepository.Get(IRepository<ProjectTask>.FilterById(id)).FirstOrDefault();
            if (task == null)
                throw new InvalidOperationException("Task not found");
            task.Performer = _userRepository.Get(IRepository<User>.FilterById(task.PerformerId)).FirstOrDefault();
            return _mapper.Map<ProjectTaskDTO>(task);
        }
        public ProjectTaskDTO Post(ProjectTaskCreateDTO task)
        {
            if (!UserIsExist(task.PerformerId))
                throw new ArgumentException("User not found");
            if (!ProjectIsExist(task.ProjectId))
                throw new ArgumentException("Project not found");
            int newId = _projectTaskRepository.Get().OrderByDescending(t => t.Id).Select(t => t.Id).FirstOrDefault() + 1;
            var newTask = new ProjectTask(newId, task.Name, task.Description, DateTime.Now, task.FinishedAt, task.State, task.ProjectId, task.PerformerId);
            _projectTaskRepository.Create(newTask);
            return _mapper.Map<ProjectTaskDTO>(newTask);
        }
        public ProjectTaskDTO Update(ProjectTaskUpdateDTO updateTask)
        {
            var task = _projectTaskRepository.Get(IRepository<ProjectTask>.FilterById(updateTask.Id)).FirstOrDefault();
            if (task == null)
                throw new InvalidOperationException("Task not found");
            if (!UserIsExist(task.PerformerId))
                throw new ArgumentException("User not found");
            if (!ProjectIsExist(task.ProjectId))
                throw new ArgumentException("Project not found");
            task.Name = updateTask.Name;
            task.Description = updateTask.Description;
            task.FinishedAt = updateTask.FinishedAt;
            task.State = updateTask.State;
            task.ProjectId = updateTask.ProjectId;
            task.PerformerId = updateTask.PerformerId;
            _projectTaskRepository.Update(task);
            return _mapper.Map<ProjectTaskDTO>(Get(updateTask.Id));
        }
        public void Delete(int id)
        {
            if (Get(id) == null)
                throw new InvalidOperationException("Task not found");
            _projectTaskRepository.Delete(id);
        }
        public IEnumerable<ProjectTaskDTO> GetTasksByUser(int userId)
        {
            if (!UserIsExist(userId))
                throw new ArgumentException("User not found");
            return GetAll().Where(t => t.Performer?.Id == userId && t.Name.Length < 45);
        }
        public IEnumerable<FinishedTaskDTO> GetFinishedTasks(int userId)
        {
            if (!UserIsExist(userId))
                throw new ArgumentException("User not found");
            return _mapper.Map<IEnumerable<FinishedTaskDTO>>(GetAll().Where(t => t.Performer?.Id == userId && t.State == TaskState.Finished && t.FinishedAt.Year == 2020));
        }
        private bool UserIsExist(int id)
        {
            if (_userRepository.Get(IRepository<User>.FilterById(id)).FirstOrDefault() == null)
                return false;
            return true;
        }
        private bool ProjectIsExist(int id)
        {
            if (_projectRepository.Get(IRepository<Project>.FilterById(id)).FirstOrDefault() == null)
                return false;
            return true;
        }
    }
}

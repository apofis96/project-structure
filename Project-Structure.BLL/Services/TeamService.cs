﻿using AutoMapper;
using Project_Structure.BLL.DTO.Team;
using Project_Structure.BLL.DTO.QueryResults;
using Project_Structure.BLL.DTO.User;
using Project_Structure.BLL.Interfaces;
using Project_Structure.DAL.Interfaces;
using Project_Structure.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Project_Structure.BLL.Services
{
    public class TeamService : ITeamService
    {
        private readonly IRepository<Team> _teamRepository;
        private readonly IRepository<User> _userRepository;
        private readonly IMapper _mapper;
        public TeamService(IRepository<Team> teamRepository, IRepository<User> userRepository, IMapper mapper)
        {
            _teamRepository = teamRepository;
            _userRepository = userRepository;
            _mapper = mapper;
        }
        public IEnumerable<TeamDTO> GetAll()
        {
            var teams = _teamRepository.Get();
            return _mapper.Map<IEnumerable<TeamDTO>>(teams);
        }
        public TeamDTO Get(int id)
        {
            var team = _teamRepository.Get(IRepository<Team>.FilterById(id)).FirstOrDefault();
            if (team == null)
                throw new InvalidOperationException("Team not found");
            return _mapper.Map<TeamDTO>(team);
        }
        public TeamDTO Post(TeamCreateDTO team)
        {
            int newId = _teamRepository.Get().OrderByDescending(u => u.Id).Select(u => u.Id).FirstOrDefault() + 1;
            var newTeam = new Team(newId, team.Name, DateTime.Now);
            _teamRepository.Create(newTeam);
            return _mapper.Map<TeamDTO>(Get(newId));
        }
        public TeamDTO Update(TeamUpdateDTO updateTeam)
        {
            var team = _teamRepository.Get(IRepository<Team>.FilterById(updateTeam.Id)).FirstOrDefault();
            if (team == null)
                throw new InvalidOperationException("Team not found");
            team.Name = updateTeam.Name;
            _teamRepository.Update(team);
            return _mapper.Map<TeamDTO>(Get(updateTeam.Id));
        }
        public void Delete(int id)
        {
            if (Get(id) == null)
                throw new InvalidOperationException("Team not found");
            _teamRepository.Delete(id);
        }
        public IEnumerable<TeamMembersDTO> GetTeamMembers()
        {
            return GetAll().Join(_userRepository.Get()
                .OrderByDescending(u => u.RegisteredAt).GroupBy(u => u.TeamId),
                t => t.Id, u => u.Key, (t, u) => new TeamMembersDTO
                {
                    Id = t.Id,
                    Name = t.Name,
                    Members = _mapper.Map<IEnumerable<UserDTO>>(u)
                }).Where(t => t.Members.All(m => (DateTime.Today.Year - m.Birthday.Year) > 10));
        }
    }
}

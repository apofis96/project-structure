﻿using AutoMapper;
using Project_Structure.BLL.DTO.ProjectTask;
using Project_Structure.BLL.DTO.QueryResults;
using Project_Structure.BLL.DTO.User;
using Project_Structure.BLL.Interfaces;
using Project_Structure.DAL.Interfaces;
using Project_Structure.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Project_Structure.BLL.Services
{
    public class UserService : IUserService
    {
        private readonly IRepository<User> _userRepository;
        private readonly IRepository<ProjectTask> _taskRepository;
        private readonly IRepository<Team> _teamRepository;
        private readonly IMapper _mapper;
        public UserService(IRepository<User> userRepository, IRepository<ProjectTask> taskRepository, IRepository<Team> teamRepository, IMapper mapper)
        {
            _userRepository = userRepository;
            _taskRepository = taskRepository;
            _teamRepository = teamRepository;
            _mapper = mapper;
        }
        public IEnumerable<UserDTO> GetAll()
        {
            var users = _userRepository.Get();
            return _mapper.Map<IEnumerable<UserDTO>>(users);
        }
        public UserDTO Get(int id)
        {
            var user = _userRepository.Get(IRepository<User>.FilterById(id)).FirstOrDefault();
            if (user == null)
                throw new InvalidOperationException("User not found");
            return _mapper.Map<UserDTO>(user);
        }
        public UserDTO Post(UserCreateDTO user)
        {
            if (user.TeamId != null && !TeamIsExist(user.TeamId.Value))
                throw new ArgumentException("Team not found");
            int newId = _userRepository.Get().OrderByDescending(u => u.Id).Select(u => u.Id).FirstOrDefault() + 1;
            DateTime registerTime = DateTime.Now;
            var newUser = new User(newId, user.FirstName, user.LastName, user.Email, user.Birthday, registerTime, user.TeamId);
            _userRepository.Create(newUser);
            return _mapper.Map<UserDTO>(newUser);
        }
        public UserDTO Update(UserUpdateDTO updateUser)
        {
            if (updateUser.TeamId != null && !TeamIsExist(updateUser.TeamId.Value))
                throw new ArgumentException("Team not found");
            var user = _userRepository.Get(IRepository<User>.FilterById(updateUser.Id)).FirstOrDefault();
            if (user == null)
                throw new InvalidOperationException("User not found");
            user.FirstName = updateUser.FirstName;
            user.LastName = updateUser.LastName;
            user.Email = updateUser.Email;
            user.Birthday = updateUser.Birthday;
            user.TeamId = updateUser.TeamId;
            _userRepository.Update(user);
            return _mapper.Map<UserDTO>(Get(updateUser.Id));
        }
        public void Delete(int id)
        {
            if (Get(id) == null)
                throw new InvalidOperationException("User not found");
            _userRepository.Delete(id);
        }
        public IEnumerable<UserTasksDTO> GetUserTasks()
        {
            return GetAll().OrderBy(u => u.FirstName)
                .GroupJoin(_taskRepository.Get().OrderByDescending(t => t.Name.Length).Join(_userRepository.Get(),
                t => t.PerformerId, u => u.Id, (t, u) =>
                {
                    t.Performer = u;
                    return t;
                }),
                u => u.Id, t => t.PerformerId, (u, t) => new UserTasksDTO
                {
                    User = u,
                    Tasks = _mapper.Map<IEnumerable<ProjectTaskDTO>>(t)
                });
        }
        private bool TeamIsExist(int id)
        {
            if (_teamRepository.Get(IRepository<Team>.FilterById(id)).FirstOrDefault() == null)
                return false;
            return true;
        }
    }
}
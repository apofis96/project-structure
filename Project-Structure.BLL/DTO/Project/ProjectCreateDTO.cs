﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Project_Structure.BLL.DTO.Project
{
    public class ProjectCreateDTO
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        public DateTime Deadline { get; set; }
        [Required]
        public int AuthorId { get; set; }
        public int TeamId { get; set; }
    }
}

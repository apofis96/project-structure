﻿using Project_Structure.BLL.DTO.ProjectTask;
using Project_Structure.BLL.DTO.Team;
using Project_Structure.BLL.DTO.User;
using System;
using System.Collections.Generic;

namespace Project_Structure.BLL.DTO.Project
{
    public class ProjectDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime Deadline { get; set; }
        public IEnumerable<ProjectTaskDTO> Tasks { get; set; }
        public UserDTO Author { get; set; }
        public TeamDTO Team { get; set; }
    }
}

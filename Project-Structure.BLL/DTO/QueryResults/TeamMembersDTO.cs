﻿using Project_Structure.BLL.DTO.User;
using System.Collections.Generic;

namespace Project_Structure.BLL.DTO.QueryResults
{
    public class TeamMembersDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public IEnumerable<UserDTO> Members { get; set; }

    }
}

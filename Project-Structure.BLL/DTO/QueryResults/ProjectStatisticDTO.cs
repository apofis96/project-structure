﻿using Project_Structure.BLL.DTO.ProjectTask;
using Project_Structure.BLL.DTO.Project;

namespace Project_Structure.BLL.DTO.QueryResults
{
    public class ProjectStatisticDTO
    {
        public ProjectDTO Project { get; set; }
        public ProjectTaskDTO LongestDescriptionTask { get; set; }
        public ProjectTaskDTO ShortestNameTask { get; set; }
        public int MembersTeamCount { get; set; }
    }
}

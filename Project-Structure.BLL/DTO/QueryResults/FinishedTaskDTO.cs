﻿namespace Project_Structure.BLL.DTO.QueryResults
{
    public class FinishedTaskDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}

﻿using Project_Structure.BLL.DTO.User;
using Project_Structure.BLL.DTO.ProjectTask;
using Project_Structure.BLL.DTO.Project;
namespace Project_Structure.BLL.DTO.QueryResults
{
    public class UserStatisticDTO
    {
        public UserDTO User { get; set; }
        public ProjectDTO LastProject { get; set; }
        public int LastProjectTasksCount { get; set; }
        public int NotFinishedTasksCount { get; set; }
        public ProjectTaskDTO LongestTask { get; set; }

    }
}

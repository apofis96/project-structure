﻿using Project_Structure.BLL.DTO.User;
using Project_Structure.BLL.DTO.ProjectTask;
using System.Collections.Generic;

namespace Project_Structure.BLL.DTO.QueryResults
{
    public class UserTasksDTO
    {
        public UserDTO User { get; set; }
        public IEnumerable<ProjectTaskDTO> Tasks { get; set; }

    }
}

﻿using Project_Structure.BLL.DTO.Project;

namespace Project_Structure.BLL.DTO.QueryResults
{
    public class TaskCountDTO
    {
        public ProjectDTO Key { get; set; }
        public int Value { get; set; }

    }
}

﻿using System.ComponentModel.DataAnnotations;

namespace Project_Structure.BLL.DTO.Team
{
    public class TeamCreateDTO
    {
        [Required]
        public string Name { get; set; }
    }
}

﻿using System.ComponentModel.DataAnnotations;

namespace Project_Structure.BLL.DTO.Team
{
    public class TeamUpdateDTO
    {
        [Required]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
    }
}

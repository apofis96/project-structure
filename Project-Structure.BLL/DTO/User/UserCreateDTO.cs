﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Project_Structure.BLL.DTO.User
{
    public class UserCreateDTO
    {
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        [Required]
        public string Email { get; set; }
        [Required]
        public DateTime Birthday { get; set; }
        public int? TeamId { get; set; }

    }
}

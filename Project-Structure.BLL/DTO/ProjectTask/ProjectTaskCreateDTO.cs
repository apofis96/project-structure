﻿using System.ComponentModel.DataAnnotations;
using Project_Structure.DAL.Models;
using System;

namespace Project_Structure.BLL.DTO.ProjectTask
{
    public class ProjectTaskCreateDTO
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        public DateTime FinishedAt { get; set; }
        [Required]
        public TaskState State { get; set; }
        [Required]
        public int ProjectId { get; set; }
        [Required]
        public int PerformerId { get; set; }
    }
}

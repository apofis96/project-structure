﻿using Project_Structure.BLL.DTO.User;
using Project_Structure.DAL.Models;
using System;

namespace Project_Structure.BLL.DTO.ProjectTask
{
    public class ProjectTaskDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime FinishedAt { get; set; }
        public TaskState State { get; set; }
        public int ProjectId { get; set; }
        public UserDTO Performer { get; set; }
    }
}

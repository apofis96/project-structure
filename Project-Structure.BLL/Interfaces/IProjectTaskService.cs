﻿using Project_Structure.BLL.DTO.ProjectTask;
using Project_Structure.BLL.DTO.QueryResults;
using System.Collections.Generic;

namespace Project_Structure.BLL.Interfaces
{
    public interface IProjectTaskService
    {
        IEnumerable<ProjectTaskDTO> GetAll();
        ProjectTaskDTO Get(int id);
        ProjectTaskDTO Post(ProjectTaskCreateDTO projectTask);
        ProjectTaskDTO Update(ProjectTaskUpdateDTO projectTask); 
        void Delete(int id);
        IEnumerable<ProjectTaskDTO> GetTasksByUser(int userId);
        IEnumerable<FinishedTaskDTO> GetFinishedTasks(int userId);
    }
}

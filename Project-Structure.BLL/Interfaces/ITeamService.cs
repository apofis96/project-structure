﻿using Project_Structure.BLL.DTO.Team;
using Project_Structure.BLL.DTO.QueryResults;
using System.Collections.Generic;

namespace Project_Structure.BLL.Interfaces
{
    public interface ITeamService
    {
        IEnumerable<TeamDTO> GetAll();
        TeamDTO Get(int id);
        TeamDTO Post(TeamCreateDTO team);
        TeamDTO Update(TeamUpdateDTO team); 
        void Delete(int id);
        IEnumerable<TeamMembersDTO> GetTeamMembers();
    }
}

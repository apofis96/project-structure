﻿using Project_Structure.BLL.DTO.Project;
using Project_Structure.BLL.DTO.QueryResults;
using System.Collections.Generic;

namespace Project_Structure.BLL.Interfaces
{
    public interface IProjectService
    {
        IEnumerable<ProjectDTO> GetAll();
        ProjectDTO Get(int id);
        ProjectDTO Post(ProjectCreateDTO project);
        ProjectDTO Update(ProjectUpdateDTO project); 
        void Delete(int id);
        IEnumerable<TaskCountDTO> GetTasksCountByUser(int userId);
        UserStatisticDTO GetUserStatistics(int userId);
        IEnumerable<ProjectStatisticDTO> GetProjectStatistics();
    }
}

﻿using Project_Structure.BLL.DTO.QueryResults;
using Project_Structure.BLL.DTO.User;
using System.Collections.Generic;

namespace Project_Structure.BLL.Interfaces
{
    public interface IUserService
    {
        IEnumerable<UserDTO> GetAll();
        UserDTO Get(int id);
        UserDTO Post(UserCreateDTO user);
        UserDTO Update(UserUpdateDTO user); 
        void Delete(int id);
        IEnumerable<UserTasksDTO> GetUserTasks();
    }
}

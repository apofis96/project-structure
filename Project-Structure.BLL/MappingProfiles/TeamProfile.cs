﻿using AutoMapper;
using Project_Structure.BLL.DTO.Team;
using Project_Structure.DAL.Models;

namespace Project_Structure.BLL.MappingProfiles
{
    public sealed class TeamProfile : Profile
    {
        public TeamProfile()
        {
            CreateMap<Team, TeamDTO>();
            CreateMap<TeamDTO, Team>();
        }
    }
}

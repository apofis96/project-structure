﻿using AutoMapper;
using Project_Structure.BLL.DTO.Project;
using Project_Structure.BLL.DTO.QueryResults;
using Project_Structure.DAL.Models;
using System.Collections.Generic;

namespace Project_Structure.BLL.MappingProfiles
{
    public sealed class ProjectProfile : Profile
    {
        public ProjectProfile()
        {
            CreateMap<Project, ProjectDTO>();
            CreateMap<ProjectDTO, Project>();
            CreateMap<KeyValuePair<ProjectDTO, int>, TaskCountDTO>()
            .ForMember(dest => dest.Key, opt => opt.MapFrom(src => src.Key))
            .ForMember(dest => dest.Value, opt => opt.MapFrom(src => src.Value));
        }
    }
}

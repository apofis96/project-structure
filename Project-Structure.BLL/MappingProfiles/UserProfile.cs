﻿using AutoMapper;
using Project_Structure.BLL.DTO.User;
using Project_Structure.DAL.Models;

namespace Project_Structure.BLL.MappingProfiles
{
    public sealed class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<User, UserDTO>();
            CreateMap<UserDTO, User>();
        }
    }
}

﻿using AutoMapper;
using Project_Structure.BLL.DTO.ProjectTask;
using Project_Structure.BLL.DTO.QueryResults;
using Project_Structure.DAL.Models;

namespace Project_Structure.BLL.MappingProfiles
{
    public sealed class ProjectTaskProfile : Profile
    {
        public ProjectTaskProfile()
        {
            CreateMap<ProjectTask, ProjectTaskDTO>();
            CreateMap<ProjectTaskDTO, ProjectTask>();
            CreateMap<ProjectTaskDTO, FinishedTaskDTO>();
        }
    }
}
